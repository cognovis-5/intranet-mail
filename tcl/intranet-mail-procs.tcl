# packages/intranet-mail/tcl/intranet-mail-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {
    
    Procedures for intranet mail
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-04-21
    @cvs-id $Id$
}

namespace eval intranet-mail {}


ad_proc -public intranet-mail::setup_imap {} {
    Once the package is correctly configured you can use this procedure to set things up with regards to imap.
} {
    # Star the session
    set session_id [imap::start_session]
    set delimiter [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPDelimiter"]

    # Check if the root folder is there
    set imap_server [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPServer"]
    set root_mailbox [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPRootFolder"]
    set unprocessed_folder [parameter::get_from_package_key -package_key "intranet-mail" -parameter "UnprocessedFolder"]

    set mailbox_list [ns_imap list $session_id \{$imap_server\} ${root_mailbox}]
    ds_comment $mailbox_list
    if {$mailbox_list eq ""} {
	# Create the mailbox
	ns_imap m_create $session_id \{$imap_server\}${root_mailbox}
    }

    # Check the project folder
    set project_imap_root_folder [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPProjectRootFolder"]
    set mailbox_list [ns_imap list $session_id \{$imap_server\} ${root_mailbox}${delimiter}${project_imap_root_folder}]
    if {$mailbox_list eq ""} {
	# Create the mailbox
	ns_imap m_create $session_id [imap::full_mbox_name -mailbox "$project_imap_root_folder"]
    }

    # Check the unprocessed folder
    set unprocessed_folder [parameter::get_from_package_key -package_key "intranet-mail" -parameter "UnprocessedFolder"]
    set mailbox_list [ns_imap list $session_id \{$imap_server\} ${root_mailbox}${delimiter}${unprocessed_folder}]
    if {$mailbox_list eq ""} {
	# Create the mailbox
	ns_imap m_create $session_id [imap::full_mbox_name -mailbox "$unprocessed_folder"]
    }


    # Create the imap folder for each project
    db_foreach project "select project_id,project_nr,project_name from im_projects where project_type_id not in ('[im_project_type_task]', '[im_project_type_ticket]','100','9500')" {
	intranet-mail::project_imap_folder_create -project_id $project_id
	ns_log Notice "Created IMAP Folder for $project_nr: $project_name"
    }
}

ad_proc -public intranet-mail::create_project_folders {
	-project_id:required
} {
	Create all necessary folders for this project
} {
	if {[imap::configured_p]} {
	
		# Check if the IMAP folder exists (how ever this is possible)
		set imap_folder [intranet-mail::project_imap_folder -project_id $project_id -check_imap]
		if {$imap_folder eq ""} {
			intranet-mail::project_imap_folder_create -project_id $project_id
		}
		
		# Check that the folder for intranet-fs exists. Create if not
		set folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
		if {$folder_id eq ""} {
			set folder_id [intranet_fs::create_project_folder -project_id $project_id]
		}
		
		set mail_folder_name [parameter::get_from_package_key -package_key "intranet-mail" -parameter "MailFolderName" -default "mail"]
		
		# Create the mail folder beneath it
		set mail_folder_id [fs::get_folder -name $mail_folder_name -parent_id $folder_id]
		if {$mail_folder_id eq ""} {
			fs::new_folder -name "$mail_folder_name" -pretty_name "$mail_folder_name" -parent_id $folder_id
		}
	}
}


ad_proc -public intranet-mail::extract_project_nrs {
    {-subject:required}
} {
    Extract all project_nrs (2007_xxxx) from the subject of an E-Mail
    
    Returns a list of project_ids (object_ids), if any are found
} {
	set line [string tolower $subject]
	regsub -all {\<} $line " " line
	regsub -all {\>} $line " " line
	regsub -all {\"} $line " " line

	set tokens [split $line " "]
	set project_nrs [list]
    
	foreach token $tokens {
	    # Tokens must be built from aphanum plus "_" or "-".
	    if {![regexp {^[a-z0-9_\-]+$} $token match ]} { continue }
        
	    # Discard tokens purely from alphabetical
	    if {[regexp {^[a-z]+$} $token match ]} { continue }

	    lappend project_nrs $token
	}

    set ids [list]
	set condition "('[join [string tolower $project_nrs] "', '"]')"
    
	set sql "
		select	project_id
		from	im_projects
		where	lower(project_nr) in $condition
	"
    return [db_list emails_to_ids $sql]
    
}

ad_proc -public intranet-mail::extract_object_ids {
    {-subject:required}
} {
    Extract all possible object_ids
    
    An Object_id can either be given by "#object_id" or with a project_nr
} {
    
    set object_ids [intranet-mail::extract_project_nrs -subject $subject]

	set line [string tolower $subject]
	regsub -all {\<} $line " " line
	regsub -all {\>} $line " " line
	regsub -all {\"} $line " " line

	set tokens [split $line " "]
    
    foreach token $tokens {
        # Figure our if this is a valid object_id
        set number [string trimleft $token "#"]
        set number [string trimright $number ":"]

	    if {![regexp {^[0-9]+$} $number match ]} { continue }
        
        # Check if this is a valid object_id
        if {[db_string object_id_p "select 1 from acs_objects where object_id = :number and object_type in ([template::util::tcl_to_sql_list [intranet-mail::valid_object_types]])" -default 0]} {
            lappend object_ids $number
        }
        
    }
    return $object_ids
}

ad_proc -public intranet-mail::valid_object_types {
} {
    return a list of valid object_types
} {
    return [list im_project im_timesheet_task im_ticket]
}


ad_proc -public intranet-mail::project_imap_folder {
    {-project_id:required}
    {-session_id ""}
    {-check_imap:boolean}
} {
    returns the IMAP Project folder, relative to IMAPRootFolder
    
    @param project_id ProjectID of the Project
    @param check_imap if passed, check that the folder exists in IMAP, otherwise return ""
} {
    set delimiter [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPDelimiter"]
    set loop 1
    set ctr 0
    set project_imap_root_folder [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPProjectRootFolder"]
    set project_folder ""
    while {$loop} {
        set loop 0
        db_1row project "select parent_id,project_nr from im_projects where project_id=:project_id"
	set project_folder "${project_nr}${delimiter}${project_folder}"
        if {$parent_id ne ""} {
            set project_id $parent_id
            set loop 1
        }
        
        # Check for recursive loop
        if {$ctr > 20} {
                set loop 0
        }
        incr ctr
    }
    set project_folder [string trimright "${project_imap_root_folder}${delimiter}$project_folder" "$delimiter"]
    if {$check_imap_p} {
	if {![imap::mailbox_exists_p -mailbox $project_folder]} {
	    set project_folder ""
	}
    }
    return $project_folder
}

ad_proc -public intranet-mail::project_imap_folder_create {
    {-project_id:required}
} {
    Creates the IMAP Project folder. Returns 1 if all went well

    @param project_id ProjectID of the Project
} {

    set project_imap_folder [intranet-mail::project_imap_folder -project_id $project_id]
    set delimiter [parameter::get_from_package_key -package_key "intranet-mail" -parameter "IMAPDelimiter"]

    # Start the IMAP Session
    set session_id [imap::start_session]

    # Don't create if it already exists....
    if {![imap::mailbox_exists_p -mailbox $project_imap_folder -session_id $session_id]} {
        set project_folder ""
        # Go through the folder components and check if this folder
        # exists. If not, create it
        foreach project_nr [split $project_imap_folder "$delimiter"] {
            
            if {$project_folder eq ""} {
                set project_folder $project_nr
            } else {
                set project_folder "${project_folder}${delimiter}$project_nr"
            }
            if {![imap::mailbox_exists_p -mailbox $project_folder -session_id $session_id]} {
                ns_log Notice "Project_folder $project_folder"
                ns_imap m_create $session_id [imap::full_mbox_name -mailbox $project_folder]
            }
        }
        imap::end_session -session_id $session_id
    }
    return 1
}

ad_proc -public im_mail_project_component {
    -project_id
    {-return_url ""}
} {
    Return the mail component for a Project (also Task / Ticket)
} {
	set context_ids $project_id
	
	# Append the invoices of this project
	set cost_ids [db_list costs "select cost_id from im_costs, im_invoices where project_id = :project_id and cost_id = invoice_id"]
	
	set context_ids [concat $context_ids $cost_ids]
    return [im_mail_object_component -context_ids $context_ids -return_url $return_url]
}

ad_proc -public im_mail_object_component {
    {-context_id ""}
    {-return_url ""}
    {-mail_url ""}
    {-party_id ""}
    {-recipient_id ""}
    {-context_ids ""}
    {-elements ""}
} {
    Return the mail component for any object
} {

    
    if {$context_ids eq ""} {
	set context_ids $context_id
    }

#    if {[llength $context_ids] <1} {
#	return ""
#	ad_script_abort
#   }
	
	if {$return_url eq ""} {
    	set return_url [im_biz_object_url [lindex $context_ids 0]]
    }
    
    set object [ns_queryget object]
    set page [ns_queryget page]
    set messages_orderby [ns_queryget messages_orderby]
    set params [list  [list return_url $return_url] [list mail_context_ids $context_ids] [list pass_through_vars [list context_id]] [list page $page] [list messages_orderby $messages_orderby] [list mail_url $mail_url] [list party_id $party_id] [list recipient $recipient_id]]
    if {$object eq ""} {
        lappend params [list object $context_id]
    }
    if {$elements ne ""} {
	    lappend params [list elements $elements]
    }
    set result [ad_parse_template -params $params "/packages/intranet-mail/lib/messages"]
    return [string trim $result]
    
}


ad_proc -public intranet-mail::package_key {} {
    The package key
} {
    return "intranet-mail"
}

ad_proc -public intranet-mail::log_add {
    {-package_id:required}
    {-sender_id ""}
    {-from_addr ""}
    {-recipient_ids:required}
    {-cc_ids ""}
    {-bcc_ids ""}
    {-to_addr ""}
    {-cc_addr ""}
    {-bcc_addr ""}
    {-body ""}
    {-message_id ""}
    {-subject ""}
    {-context_id ""}
    {-file_ids ""}
    {-filesystem_files ""}
} {
    Insert new log entry

    @param sender_id party_id of the sender
    @param from_addr e-mail address of the sender. At least party_id or from_addr should be given
    @param recipient_ids List of party_ids of recipients
    @param cc_ids List of party_ids for recipients in the "CC" field
    @param bcc_ids List of party_ids for recipients in the "BCC" field
    @param to_addr List of email addresses seperated by "," who recieved the email in the "to" field but got no party_id
    @param cc_addr List of email addresses seperated by "," who recieved the email in the "cc" field but got no party_id
    @param bcc_addr List of email addresses seperated by "," who recieved the email in the "bcc" field but got no party_id
    @param body Text of the message
    @param message_id Message_id of the email
    @param subject Subject of the email
    @param context_id Context in which this message was send. Will replace object_id
    @param file_ids Files send with this e-mail
    @param filesystem_files Files send from the Filesystem
} {
    set creation_ip [ad_conn peeraddr]
    set creation_user [ad_conn user_id]
    
    # the object_id passed in the API parameters is the project_id. Moreover we must assign it as context_id.
    set log_id [db_nextval "acs_object_id_seq"]	

    db_exec_plsql insert_acs_mail_log {
	SELECT acs_mail_log__new (
                                  :log_id,
                                  :message_id,
                                  :sender_id,
                                  :package_id,
                                  :subject,
                                  :body,
                                  :creation_user,
                                  :creation_ip,
                                  :context_id,
                                  :cc_addr,
                                  :bcc_addr,
                                  :to_addr,
                                  :from_addr
                                  )
    }



    foreach file_id $file_ids {
		set item_id [content::revision::item_id -revision_id $file_id]
		if {$item_id eq ""} {
		    set item_id $file_id
		}
		db_dml insert_file_map "insert into acs_mail_log_attachment_map (log_id,file_id) values (:log_id,:file_id)"
    }

    # Now add the recipients to the log_id
    
    foreach recipient_id $recipient_ids {
		db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'to')}
    }

    foreach recipient_id $cc_ids {
		db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'cc')}
    }

    foreach recipient_id $bcc_ids {
		db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'bcc')}
    }

	# Add the filesystem_files as we can't map them like the other files
	db_dml filesystem_file_update "update acs_mail_log set filesystem_files = :filesystem_files where log_id = :log_id"
	
    return $log_id
}

ad_proc -public intranet-mail::load_mails {} {
    Scheduled procedure that will scan for mails
} {
    # SemP: Only allow one process to process...
    if {[nsv_incr intranet-mail_load_mails check_mails_p] > 1} {
		nsv_incr intranet-mail_load_mails check_mails_p -1
		return
    }
	
    catch {
		ns_log Notice "intranet-mail_load_mails.scan_mails: about to load qmail queue"
		imap::load_mails
    } err_msg

    # SemV: Release Semaphore
    nsv_incr intranet-mail_load_mails check_mails_p -1
}

ad_proc -public intranet-mail::link_mails {
	-session_id:required
	-array:required
} {
	Procedure to call for imap::incoming_email callback
	
	Link mails to the project and users

	@author Malte Sussdorff (malte.sussdorff@cognovis.de)
	@creation-date 2011-04-22

	@param array        An array with all headers, files and bodies. To access the array you need to use upvar.
	@return             nothing
	@error
} {
	# get a reference to the email array
	upvar $array email

	set from_addr [lindex $email(from) 1]
	set from_party_id [lindex $email(from) 0]
	if {$from_party_id ne ""} {
		# We don't need to log the sender twice
		set from_addr ""
	}

	# By default, assume you can deal with the mail
	set unprocessed_p 0

	# Figure out the object_id from the subject
	set object_ids [intranet-mail::extract_object_ids -subject $email(subject)]
	ns_log Notice "Loading imap mails:: $object_ids"
	if {$object_ids eq ""} {
		# We did not find an object_id in the subject.
		# Check if the from_addr has a valid SLA
		if {$from_party_id eq ""} {
			# We don't have an object_id and we can't link the mail to
			# a sender, keep it unprocessed.
			set unprocessed_p 1
		} else {
			# Check if the sender is an employee. If he is, move to unprocessed directly.
			if {[im_user_is_employee_p $from_party_id]} {
			  set unprocessed_p 1
			} else {
				# Find out the company
				set company_id [db_string get_company_id "select object_id_one from acs_rels ar, registered_users ru, im_companies ic where ar.object_id_one = ic.company_id and ar.object_id_two = ru.user_id and ru.user_id = :from_party_id" -default ""]
				if {$company_id ne ""} {
					# Find the SLA
					set object_ids [db_string get_sla_project_id "select project_id from im_projects where company_id = :company_id and project_type_id = [im_project_type_sla] order by project_id asc limit 1" -default ""]
				} else {
					set object_ids ""
				}
				if {$object_ids eq ""} {
					set unprocessed_p 1
				}
		   }
	   }
	}

	# Get some defaults
	set sequence_nr $email(sequence_nr)
	set unprocessed_folder [parameter::get_from_package_key -package_key "intranet-mail" -parameter "UnprocessedFolder"]

	# Now we know if we can process the message
	if {$unprocessed_p eq 1} {
		imap::move_mail -session_id $session_id -destination_mailbox $unprocessed_folder -sequence_nr $sequence_nr

		return
	}

	set html_body ""
	set plain_body ""
	foreach email_body $email(bodies) {
		set mime_type [lindex $email_body 0]
		set body_content [lindex $email_body 1]
		if {$mime_type eq "text/html"} {
			append html_body $body_content
		} else {
			append plain_body $body_content
		}
	}

	# Save HTML by default
	if {$html_body ne ""} {
		set body $html_body
	} else {
		set body $plain_body
	}

	# We only get the first object_id, might be expanded in the future
	set object_id [lindex $object_ids 0]

	# find the imap folder. This should be a project
	if {[acs_object_type $object_id] ne "im_project"} {
		# Get the parent, which should be a project
		set project_id [db_string parent_id "select parent_id from im_projects where project_id = :object_id"]
	} else {
		set project_id $object_id
	}

	set imap_folder [intranet-mail::project_imap_folder -project_id $project_id -check_imap]
	if {$imap_folder eq ""} {
		# Throw an error
		ns_log error "No IMAP Folder for object $object_id"
		imap::move_mail -session_id $session_id -destination_mailbox $unprocessed_folder -sequence_nr $sequence_nr
		return
	}

	# Find the folder for the project, but only if we deal with files
	if {$email(files) ne ""} {

		# Find out the folder_id.
		# It is either directly linked to the project or to it's parent.

		# We only deal with projects at the moment, in case this get's
		# expanded we definitely need some reworking here as well as
		# intranet-fs
		set package_id [im_package_core_id]

		set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id -try_parent]
		set mail_folder_name [parameter::get_from_package_key -package_key "intranet-mail" -parameter "MailFolderName" -default "mail"]
		set folder_id [fs::get_folder -name $mail_folder_name -parent_id $project_folder_id]
		if {$folder_id eq ""} {
			# No folder found, throw an error and put the files into
			# the root folder
			ns_log error "No Mail Folder for the attachments..."
			set folder_id [intranet_fs::get_projects_root_folder_id -package_id $package_id]
		}
	}

	# Deal with the files
	set files ""
	set file_ids ""
	# As this is a connection less callback, set the IP Address to
	# local
	set peeraddr "127.0.0.1"

	foreach file $email(files) {
		set mime_type [lindex $file 0]
		set file_title [lindex $file 1]
		set file_path [lindex $file 2]

		# We need a creation user for the content item, so use the
		# sender or the sysadmin.
		if {$from_party_id ne ""} {
			set creation_user $from_party_id
		} else {
			set creation_user [im_sysadmin_user_default]
		}

		set existing_item_id [content::item::get_id_by_name -name $file_title -parent_id $folder_id]
		if {$existing_item_id ne ""} {
			set item_id $existing_item_id
		 } else {
			set item_id [db_nextval "acs_object_id_seq"]
			content::item::new -name $file_title \
				-parent_id $folder_id \
				-item_id $item_id \
				-package_id $package_id \
				-creation_ip 127.0.0.1 \
				-creation_user $creation_user \
				-title $file_title
		}

		set revision_id [content::revision::new \
							 -item_id $item_id \
							 -tmp_filename $file_path\
							 -creation_user $creation_user \
							 -creation_ip 127.0.0.1 \
							 -package_id $package_id \
							 -title $file_title \
							 -description "File send by e-mail from $email(from) to $email(to) on subject $email(subject)" \
							 -mime_type $mime_type]

		file delete $file_path
		lappend file_ids $revision_id
	}

	# Check if we already logged this mail for this object_id
	set message_id $email(message-id)
	if {![db_string logged_p "select 1 from acs_mail_log where object_id = :object_id and message_id = :message_id" -default 0]} {

		set package_id [apm_package_id_from_key "intranet-mail"]
		set log_id [intranet-mail::log_add -package_id $package_id \
						-sender_id $from_party_id \
						-from_addr $from_addr \
						-recipient_ids [lindex $email(to) 0] \
						-cc_ids [lindex $email(cc) 0] \
						-bcc_ids [lindex $email(bcc) 0] \
						-to_addr [lindex $email(to) 1] \
						-cc_addr [lindex $email(cc) 1] \
						-bcc_addr [lindex $email(bcc) 1] \
						-body $body \
						-message_id $message_id \
						-subject $email(subject) \
						-file_ids $file_ids \
						-context_id $object_id]

		# Execute the callback for logged E-Mails. This is great for
		# custom callbacks triggering workflows etc.
		callback intranet-mail::logged_email -log_id $log_id
	}

	# Move the mail to the correct imap folder
	imap::move_mail -session_id $session_id -destination_mailbox $imap_folder -sequence_nr $sequence_nr

}

